#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = 'Vincent DELBAR'
__version__ = 0.2

import os
import sys
import argparse
from pathlib import Path
from functools import partial
from ast import literal_eval
from multiprocessing import Pool

from numpy import array_split
from pandas import concat

import geopandas as gpd
from shapely.errors import TopologicalError


def _maxoverlap(array, overlay):
    """Return the index of the most overlapping feature"""
    geom = array[-1]
    matches_index = overlay.index[overlay.intersects(geom)]
    matches = overlay[overlay.index.isin(matches_index)]
    m = matches.shape[0]  # number of matching features
    if m == 0:
        return None
    if m == 1:
        return matches_index.values[0]
    if m > 1:
        return matches.intersection(geom).area.idxmax()


# Multiproceessing
def _mp_maxoverlap(overlay, splitdf):
    """Special function with a chunk as data input - second argument"""
    return splitdf.apply(_maxoverlap, args=(overlay,), axis=1, raw=True)


def _parallelize(func, df, n_cores, chunks):
    """Split data, create a pool of workers, compute results in parallel and concat results"""
    data_split = array_split(df, chunks)
    pool = Pool(n_cores)  
    data = concat(pool.map(func, data_split))
    pool.close()
    pool.join()

    return data


# Core function
def sjoin_maxoverlap(left, right, name, j_fields, n_cores=1, chunks=None):
    """Join all columns in right table using max overlapping area, single or multicore"""
    tmp_res = None
    if n_cores == 1:
        tmp_res = left.apply(_maxoverlap, args=(right,), axis=1, raw=True)
    elif n_cores > 1:
        # Create a function to apply, only one argument (the splitted data)
        _func = partial(_mp_maxoverlap, right)
        tmp_res = _parallelize(_func, left, n_cores, chunks)
    if tmp_res.shape[0] == 0:
        print('Intersection is empty. May be check your SRS.')
        return

    results = left.join(tmp_res.rename(name))
    if not j_fields:
        return results
    return results.join(right[j_fields], on=name)


def _ck_output_param(output):
    """Check if output format is supported or exit, return driver expected by geopandas"""
    drivers = {'shp': 'ESRI Shapefile',
               'gpkg': 'GPKG',
               'geojson': 'GeoJSON',
               'json': 'GeoJSON'}
    try:
        ext = Path(output).name.split('.')[-1]
        ext = ext.lower()
    except IndexError:
        output += '.shp' ; ext = 'shp'
    if ext not in drivers.keys():
        print(f"{ext.upper()} file format is not allowed; using shapefile instead")
        output = output.replace('.' + ext, '.shp') ; ext = 'shp'
    return drivers[ext], Path(output)


def _ck_fields_param(j_data, j_fields):
    """Taking all columns by default if none is given, if 'index' we only keep target geometries index"""
    if j_fields == 'index':
        return False
    if not j_fields or j_fields[0] in ('', 'ALL') :
        j_fields = list(j_data.columns)
        j_fields.remove('geometry')
    return j_fields


def _ck_chunks_param(workers_p, chunks_p, df_length):
    """Fix param number of chunks, compute new size if < 1 or greater than dataset size"""
    val = chunks_p
    if workers_p > 1:
        if chunks_p >= df_length:
            val = int(df_length / workers_p)
        if isinstance(chunks_p, float) and 0. < chunks_p <= 1.:
            val = int(chunks_p * df_length)
        if val < workers_p:
            val = workers_p
    else:
        val = None
    return val


def main(args):
    name = args.name
    driver, outpath = _ck_output_param(args.output)
    workers = args.workers
    left = gpd.read_file(args.left)
    right = gpd.read_file(args.right)
    epsg = left.crs.to_epsg()
    if left.crs.to_epsg() != right.crs.to_epsg():
        return f"""Different SRS for left and right layers is not allowed, try to reproject
                right layer to ESPG "{epsg if epsg else '?'}" and make sure to fix geometries."""
    fields = _ck_fields_param(right, args.fields.split(','))

    chunks = _ck_chunks_param(workers, literal_eval(args.chunks), len(left))
    print(f"Processing {chunks} chunks with a pool of {workers} workers...")
    try:
        joint = sjoin_maxoverlap(left, right, name, fields, workers, chunks)
        if joint is None:
            return "Unexpected error, empy result. Exiting."
    except TopologicalError as e:
        return str(e) + "\nMay be fix your geometries and try again."

    print(f"Writing output to {outpath}")
    joint.to_file(str(outpath), driver=driver)

    return 0


parser = argparse.ArgumentParser(description="""Spatial join between two polygon layers using
maximum area of intersections if more than one candidate feature.""")
parser.add_argument('left', help="input layer")
parser.add_argument('right', help="input overlay / right layer for spatial join")
parser.add_argument('output', help="output file with extension (SHP, GPKG or GeoJSON)")
parser.add_argument('-f', '--fields', default='', metavar='',
                    help='field(s) to keep from right layer (n or n,n,n,n), keep all by default')
parser.add_argument('-n', '--name', default='jdx', metavar='',
                    help='name for the joint index field, "jdx" by default')
parser.add_argument('-w', '--workers', default=os.cpu_count(), type=int, metavar='',
                    help=f'integer number of workers to enable parallel computing, w * number of CPU if float < 1., default is all cores')
parser.add_argument('-c', '--chunks', default='0.01', metavar='',
                    help='number of chunks, using dataset size / n_cores if value is too high or c * size of dataset if float, default to 1/100')

if __name__ == '__main__':
    sys.exit(main(parser.parse_args()))
