# -*- coding: utf-8 -*-
from distutils.core import setup

setup(name='Spatial join by max overlap',
      scripts=['sjoin_maxoverlap.py'],
      version='0.2',
      description="""Spatial join between two polygon layers using
                  maximum area of intersections if more than one candidate feature.""",
      # long_description='Boite à outils de géotraitements',
      author='Vincent Delbar',
      author_email='vincent.delbar@latelescop.fr',
      license='None',
      platforms=['Linux', 'Mac', 'Windows'])
